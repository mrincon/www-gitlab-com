---
layout: markdown_page
title: "Recruiting Process - Coordinator Tasks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiting Process Framework - Coordinator Tasks
{: #framework-coord}

### Step 11/C: Schedule first interview

Once a candidate has provided their availability in Greenhouse, the coordinator will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) to determine scheduling needs, in this case, scheduling a first round interview the hiring manager. If the repo is outdated or you are unsure of the interview process, reach out to the recruiter.

### Step 14/C: Schedule team interviews

Once a candidate has provided their availability in Greenhouse for the second-round interviews, the coordinator will utilize the internal [hiring processes repository](https://gitlab.com/gitlab-com/people-ops/hiring-processes) and schedule all second-round interviews with the interview team. These interviews will be scheduled at the same time, to take place either on the same day (ideal) or over the course of a 2-3 days. The coordinator should be sure to inform the candidate that each subsequent interview is contingent on the success of the prior interview.

#### Executive Interview Scheduling

VP's
  - The coordinators can schedule as normal and do not need to loop in Executive Assistants (EA).

C-Suite (CFO, CPO, CMO, CRO)
  - The recruiter moves the candidate to the Executive Interview stage in Greenhouse.
  - The recruiter pings the EA supporting the executive in Greenhouse to ask for available time slots and includes any relevant details such as it is an internal candidate or high priority.
  - The EA pings the coordinator in Greenhouse with 3 available time slots.
  - The coordinator sends the candidate the suggested times, which are only visible via the online calendar tools and do not show in the email itself.
  - The coordinator schedules the interview after the candidate responds with the chosen time, then they send out the interview confirmation to the candidate and cc the EA on the email.

CEO
  - The coordinator requests the candidate's availability through Greenhouse.
  - Once the availability is received, the coordinator pings the CEO's EA who handles the rest of the scheduling.

#### Interview Reschedule Requests and Other Communication to the CES Email 

The CES team utilizes [GitLab Service Desk](https://about.gitlab.com/product/service-desk/) to track incoming emails to the CES email alias. 

1. Under this [CES Service Desk Project](https://gitlab.com/gl-recruiting/ces-service-desk) set up the proper notifications
   - Click on the bell icon on the top right next to Star and Clone
   - Go to Custom Settings
   - Check "New issue"
   - Closeout the window
1. On the left-side menu bar click Issues
   - This is where all our incoming CES emails will create an issue. You'll get an alert when someone sends an email to the CES email alias. Any "emails" that need to be addressed will be an open issue listed within this project.
1. Click on the new Issue
1. Assign it to yourself on the right-side toolbar
1. Read the Issue message
1. Add the appropriate label
1. Respond to the "email" by adding comments to the issue (be sure to enter comments as you would an email to the candidate)
   - The response might be as simple as Dear x, thank you for giving the team a heads up on the change of your schedule. I've canceled your interview and we will reach out to reschedule after taking a look at schedules.
1. If this one comment address the entire message and the Application Tracking System (ATS) is the tool needed, add a comment and close the issue
   - If the one comment does not address the entire message then only select comment.
1. Navigate to the ATS, if you are unable to reschedule or reset-up yourself tag the appropriate CES in the applicable greenhouse profile notes and copy the issue link in the comment (i.e. @Jane please reschedule with John per https:xxxxx)

There may be situations where the CES email is used to thank the team or to send a follow-up note. In those cases, we would copy the text of the issue to forward to the appropriate parties through a greenhouse profile at mention.

### Step 20/C: Initiate background check

Once notified by the recruiter, the coordinator will [initiate a background check](https://about.gitlab.com/handbook/people-operations/code-of-conduct/#initiating-a-background-check) for the candidate. The coordinator will continue to monitor the background check until finalized. Once finalized, the coordinator will notify the recruiter that it is completed. If the background check has red flags, the coordinator will loop in the People Ops Generalist and Senior Director of Legal Affairs for future action. Some flags, such as incorrect start/end dates at previous companies can be investigated by the coordinator; if consistencies are still found, then the check can be escalated. Driving-related offenses are not considered flags at GitLab and can be ignored.

### Step 23/C: Send contract

Once the verbal offer is made, the coordinator will send the contract to the applicant, using DocuSign in Greenhouse. On rare occasion, the coordinator may have to create the contract outside of Greenhouse using Google Docs; if this is the case, the coordinator needs to have a People Ops team member review the contract for accuracy before sending it out for signature.
